import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  stats: any;
  chartConf = {
    view: [350, 350],
    doughnut: false,
    results: [
      {
        name: 'TEMP',
        value: 100
      }
    ],
    showLegend: false,
    showLabels: true,
    explodeSlices : false,
    gradient: false,
    // rose for girls, blue for boys no time to choose other non sexist colors.
    customColors: [
      {
        name: 'GIRLS',
        value: '#e62295'
      },
      {
        name: 'BOYS',
        value: '#1559ff'
      }
    ]
  };
  resultInAverage = [];
  resultOut = [];
  resultOutAverage = [];
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get('https://app-jlt.azurewebsites.net/api/HttpTriggerJS1?code=agrY5yfloeu2kBejQHMVwgzqbxLsve57/G6sndqL5janS7tBHL7lJA==')
      .subscribe((r) => {
        this.stats = r;
        this.chartConf.results = [{
            name: 'GIRLS',
            value: this.stats.in.females.sum,
        },
        {
          name : 'BOYS',
            value: this.stats.in.males.sum
        }];
        this.resultInAverage = [{
          name: 'GIRLS',
          value: this.stats.in.females.average,
        },
          {
            name : 'BOYS',
            value: this.stats.in.males.average,
          }];

        this.resultOut = [{
          name: 'GIRLS',
          value: this.stats.out.females.sum,
        },
          {
            name : 'BOYS',
            value: this.stats.out.males.sum,
          }];
        this.resultOutAverage = [{
          name: 'GIRLS',
          value: this.stats.out.females.average,
        },
          {
            name : 'BOYS',
            value: this.stats.out.males.average,
          }];
    });
  }
}
